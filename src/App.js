import React from 'react';

import GlobalStyle from './assets/styles/global'
import TopBar from './components/TopBar'
import HeaderSite from './components/Header'
import AllCards from './components/cards'
import ContatoSection from './components/Contato'
import Rodape from './components/Footer'
function App() {
  return (
    <div className="App">
        <GlobalStyle />
        <TopBar/>
        <HeaderSite />
        <AllCards />
        <ContatoSection />
        <Rodape />
    </div>
  );
}

export default App;
