import React, {useEffect} from "react";
import { Header, CardCentro, Mouse, MouseIcon, ArrowDown } from "./styled";

import imgCard from "../../assets/img/Imagemcard.png";

//animacoes
import AOS from 'aos';
import 'aos/dist/aos.css';




export default function HeaderSite() {
    
    useEffect(() => {
         AOS.init({
      duration : 1000
    })
    })

    return (
        <Header>
            <CardCentro>
                <div className="infoCard">
                    <h3 data-aos="fade-right">Transistor - Red Singer</h3>
                </div>
                <div className="imagemCard" data-aos="fade-up" >
                    <img src={imgCard} alt="transistor - red the singer" />
                </div>

                <div className="ompeao">
                    <p data-aos="fade-down">
                        "Olha, o que quer que você esteja pensando, me faça um
                        favor, não solte."
                    </p>
                </div>

                <Mouse>
                    <MouseIcon>
                        <div className="wheel"></div>
                    </MouseIcon>
                </Mouse>
                <ArrowDown>&#11139;</ArrowDown>
            </CardCentro>
        </Header>
    );
}
