import styled from "styled-components";

//images
import fundoImg from "../../assets/img/fundo.png";
import gradienteFundo from "../../assets/img/Imagemcard.svg";

export const Header = styled.header`
    height:960px;
background:url(${fundoImg}) center center no-repeat ;
background-size:cover;
display:flex;
justify-content:center;
align-items:flex-end;
flex-direction:row;
padding:0px;



.ompeao{
    max-width: 540px;
    height auto;
    width:540px;
    display:flex;
    justify-content:center;
    margin:0px auto 60px auto;
    position:relative;
    z-index:2;
    p{
    letter-spacing: 0px;
    color: #F0F0F2;
    text-shadow: 0px 3px 6px #00000029;
    font-size:20px;
    text-align:center;
    max-width:270px;
    margin-top:15px;
}
}
    @media(max-width:1024px){
    height: 540px;
    

.ompeao{
    max-width: 100%;
    height auto;
    width:100%;
    display:flex;
    justify-content:center;
    margin:0px auto 10px auto;
    position:relative;
    z-index:2;
    p{
    letter-spacing: 0px;
    color: #F0F0F2;
    text-shadow: 0px 3px 6px #00000029;
    font-size:18px;
    text-align:center;
    max-width:80%;
    margin-top:-15px;
}
}

    }
`;
export const CardCentro = styled.div`
    height: 100.6%;
    display: flex;
    justify-content: space-between;
    flex-direction: column;
    background: url(${gradienteFundo}) no-repeat top center;

    .imagemCard {
        max-height: 100%;
        display: flex;
        justify-content: center;
        img {
            height: 100%;
            margin: 0px auto 20px auto;
            max-width: 520px;
            object-fit: contain;
        }
    }
    .infoCard {
        display: block;
        margin: 12px 0px 0 20px;
        h3 {
            color: #fff;
            font-weight: bold;
            font-size: 18px;
            text-transform: uppercase;
            position: relative;
            top: 12px;
            margin-bottom: 10px;
        }
    }

    @media (max-width: 1024px) {
        height: 100.7%;
        width: 100%;
        display: flex;
        justify-content: space-between;
        flex-direction: column;
        background: url(${gradienteFundo}) no-repeat top center;

        .imagemCard {
            max-height: 100%;
            display: flex;
            justify-content: center;
            width: 90%;
            margin: 20px auto;
            img {
                height: auto;
                text-align: center;
                max-width: 90%;
                width: 90%;
                object-fit: contain;
            }
        }
        .infoCard {
            text-align: left;
            display: flex;
            justify-content: center;
            h3 {
                color: #fff;
                font-weight: bold;
                font-size: 18px;
                text-transform: uppercase;
                position: relative;
                top: 12px;
                margin-bottom: 10px;
                margin: 0 auto;
            }
        }
    }
`;

export const MouseIcon = styled.div`
    position: relative;
    border: 2px solid #fff;
    border-radius: 16px;
    height: 50px;
    width: 30px;
    margin: 0 auto;
    display: block;
    z-index: 10;
    background: #fff;
    position: relative;
    top: -35px;

    @media (max-width: 1024px) {
        position: relative;
        border: 2px solid #fff;
        border-radius: 16px;
        height: 50px;
        width: 30px;
        margin: 0 auto;
        display: block;
        z-index: 10;
        background: #fff;
        position: relative;
        top: 5px;
    }
    .wheel {
        -webkit-animation-name: drop;
        -webkit-animation-duration: 1s;
        -webkit-animation-timing-function: linear;
        -webkit-animation-delay: 0s;
        -webkit-animation-iteration-count: infinite;
        -webkit-animation-play-state: running;
        animation-name: drop;
        animation-duration: 1s;
        animation-timing-function: linear;
        animation-delay: 0s;
        animation-iteration-count: infinite;
        animation-play-state: running;
    }

    .wheel {
        position: relative;
        border-radius: 10px;
        background: #191919;
        width: 4px;
        height: 10px;
        top: 4px;
        margin-left: auto;
        margin-right: auto;
    }

    @-webkit-keyframes drop {
        0% {
            top: 5px;
            opacity: 0;
        }
        30% {
            top: 10px;
            opacity: 1;
        }
        100% {
            top: 25px;
            opacity: 0;
        }
    }

    @keyframes drop {
        0% {
            top: 5px;
            opacity: 0;
        }
        30% {
            top: 10px;
            opacity: 1;
        }
        100% {
            top: 25px;
            opacity: 0;
        }
    }
`;

export const Mouse = styled.div`
    display: block;
    justify-content: center;
    text-align: center;
`;

export const ArrowDown = styled.div`
    display: block;
    justify-content: center;
    text-align: center;
    position: relative;
    top: -25px;
    transform: rotate(-90deg);
    color: #fff;
`;
