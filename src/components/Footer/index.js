import React from "react";
import { Footer, ButtonTop } from "./styled.js";
const Rodape = () => {
    return (
        <Footer>
            <ButtonTop href="#topo"><span>&#11139;</span></ButtonTop>
        </Footer>
    );
};
export default Rodape;
