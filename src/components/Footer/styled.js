import styled from "styled-components";

export const Footer = styled.footer`
    background: #363636;
    padding: 150px 100px;

@media(max-width:1024px){
    padding: 80px 30px;
    display:flex;
    justify-content:center;
align-items:center;
align-content:center;
}
`;

export const ButtonTop = styled.a`
    background: #fff;
    color: #363636;
    padding: 10px;
    height: 80px;
    width: 80px;
    border-radius: 100px;
    color: #363636;
text-align:center;
    float: right;
    text-decoration: none;
    transform: rotate(90deg);
    font-size: 38px;
    display:flex;
    justify-content: center;
align-items:center;
align-content:center;

    
span{
    display:block;
    width:100%;
    text-align:center;
    position:relative;
    top:-5px;
}
    &:hover{
        background:#58B790;
        color:#fff;
    }


@media (max-width:760px){
    float:none;

    margin:0 auto;
    background: #fff;
    color: #363636;
    padding: 10px;
    height: 50px;
    width: 50px;
    border-radius: 100px;
    color: #363636;
text-align:center;
    text-decoration: none;
    transform: rotate(90deg);
    font-size: 28px;
    display:flex;
    justify-content: center;
align-items:center;
align-content:center;
}

`;
