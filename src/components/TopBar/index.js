import React from "react";
import {TopBar} from './styled'
import miniIcon from '../../assets/img/mini-icon.png'

export default function Top(){
    return(

    <TopBar id="topo"><img src={miniIcon} alt="supergiantegames" /><h2 data-aos="fade-in"> Supergiantgames</h2></TopBar>
    )
}

