import styled from "styled-components";

export const TopBar = styled.div`
    background: #363636 0% 0% no-repeat padding-box;
    color: #fff;
    height: 70px;
    display: flex;
    justify-content: center;
    align-items: center;

img{
    object-fit:contain
    max-height:100px;
    height:100px;
    margin-right:10px;
}
h2{
    text-transform:uppercase;
    font-weight:bold;
    font-size:18px;
    text-align:left;
    
}
`;
