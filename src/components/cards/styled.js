import styled from "styled-components";

export const SectionCards = styled.section`
    background: #363636;
    height: 100%;
padding:70px 0px 170px 0px;
display:flex;
align-items:center;
justify-content:Center;


@media(max-width:1024px){
    
display:flex;
align-items:center;
justify-content:Center;
flex-direction:column;
}
`;

export const Card = styled.div`
    background: #fff;
    max-width: 400px;
    width: 400px;
display:block;
    border-radius: 30px;
    height: 600px;
margin:300px 20px 0px 20px; 

    .backImg {
        background: #363636;
        border: 4px solid #fff;
        border-radius: 100px;
        position: relative;
        top: -100px;
        height: 440px;
        width: 370px;
        margin: 0 auto;
        img {
            position: absolute;
            width: 400px;
            margin-top: -97px;
            text-align: center;
            margin-left: -19px;
        }
    }
    .descricaoCard {
        padding: 0px 30px;
        position: relative;
        top: -70px;
        p {
            font-size: 20px;
            color: ##363636;
            text-align: left;
        }
    }

    @media (max-width: 760px) {
        background: #fff;
        max-width: 80%;
        margin: 100px 50px 0px 50px;
        border-radius: 30px;
        height: 320px;

        .backImg {
            background: #363636;
            border: 4px solid #fff;
            border-radius: 50px;
            position: relative;
            top: -50px;
            height: 160px;
            width: 90%;
            margin: 0 auto;
            display:flex;
            flex-direction:column;
            justify-content:center;
            text-align:center;
            img {
                position: absolute;
                width:auto;
                height: auto;
                margin-top: -40px;
                margin-left:auto;
                margin-right:auto;
                text-align: center;
                margin-left: -9px;
                max-height:200px;
            }
        }
        .descricaoCard {
            padding: 0px 20px;
            position: relative;
            top: -30px;
            p {
                font-size: 16px;
                text-align: left;
            }
        }
    }
`;
