import React from "react";
import grant from "../../assets/img/Grant.png";
import red from "../../assets/img/Red.png";
import sybil from "../../assets/img/Sybil_2.png";
import { SectionCards } from "./styled.js";
import { Card } from "../../components/cards/styled.js";
export default function AllCards() {
    return (
        <SectionCards>
        <Card data-aos="fade-up">
            <div className="imageCard">
                <div className="backImg">
                    <img src={grant} alt="Grant" />
                </div>
            </div>
            <div className="descricaoCard">
                <p>
                    A Camerata foi apenas os dois no início, e suas fileiras
                    nunca foram destinadas a exceder um número a ser contado em
                    uma mão.
                </p>
            </div>
        </Card>
        <Card data-aos="fade-up" data-aos-duration="2000">
            <div className="imageCard">
                <div className="backImg">
                    <img src={red} alt="Red" />
                </div>
            </div>
            <div className="descricaoCard">
                <p>
                    Red, uma jovem cantora, entrou em posse do Transistor. Sendo
                    a poderosa espada falante. O grupo Possessores quer tanto
                    ela quanto o Transistor e está perseguindo implacavelmente a
                    sua procura.
                </p>
            </div>
        </Card>
        <Card data-aos="fade-up" data-aos-duration="3000">
            <div className="imageCard">
                <div className="backImg">
                    <img src={sybil} alt="Sybil" />
                </div>
            </div>
            <div className="descricaoCard">
                <p>
                    Sybil é descrita pelo Transistor como sendo os "olhos e
                    ouvidos" da Camerata.
                </p>
            </div>
        </Card>
        </SectionCards>
    );
}
