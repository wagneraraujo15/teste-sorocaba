import React, { useState } from "react";
import {
    Contato,
    Form,
    ContentForm,
    InputForm,
    Textarea,
    MensagemError,
    MensagemSucesso
} from "./styled";
const ContatoSection = () => {
    const [inputForm, setInputForm] = useState({
        nome: "",
        email: "",
        mensagem: ""
    });
    const [mensagem, setMensagem] = useState(false);
    const [mensagemSucesso, setMensagemSucesso] = useState(false);

    const handleInput = e => {
        setInputForm({
            ...inputForm,
            [e.target.name]: e.target.value
        });
        // console.log(e.target.value)
    };

    const SendForm = e => {
        e.preventDefault();
        if (
            inputForm.nome === "" ||
            inputForm.email === "" ||
            inputForm.mensagem === ""
        ) {
            setMensagem(true);
            return false;
        } else {
            setMensagem(false);
            setMensagemSucesso(true);
        }

        console.log(
            `Dados para enviar: ${inputForm.nome}, ${inputForm.email}, ${inputForm.mensagem}`
        );
    };
    return (
        <Contato >
            <ContentForm data-aos="fade-up" >
                <h2>Formulário</h2>
                <p>
                    Adipisicing dolores in voluptas cumque soluta. Quis fuga
                    eius odio quae voluptate? Architecto mollitia dicta ad ab
                    perspiciatis? Nam autem consequatur deleniti dolores
                    tempore. Quam earum ex accusamus vel
                </p>
                <Form onSubmit={SendForm}>
                    <InputForm>
                        <input
                            id=""
                            type="text"
                            name="nome"
                            placeholder="Nome"
                            onChange={handleInput}
                        />
                    </InputForm>
                    <InputForm>
                        <input
                            id=""
                            type="email"
                            name="email"
                            placeholder="Email"
                            onChange={handleInput}
                        />
                    </InputForm>

                    <Textarea
                        id=""
                        name="mensagem"
                        placeholder="Mensagem"
                        cols="5"
                        rows="9"
                        onChange={handleInput}
                    />

                    <button className="enviar" type="submit">
                        Enviar
                    </button>
                </Form>
                {mensagem && (
                    <MensagemError>
                        Campo vázio, preencha, por favor
                    </MensagemError>
                )}
                {mensagemSucesso && (
                    <MensagemSucesso>
                        Mensagem enviada com sucesso, obrigado{" "}
                    </MensagemSucesso>
                )}
            </ContentForm>
        </Contato>
    );
};
export default ContatoSection;
