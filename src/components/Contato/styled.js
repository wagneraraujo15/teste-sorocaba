import styled from "styled-components";
export const Contato = styled.section`
    background: transparent linear-gradient(143deg, #7dede2 0%, #58b790 100%) 0%
        0% no-repeat padding-box;
    height: 550px;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
`;
export const ContentForm = styled.div`
    height: 830px;
    background: #fff;
    border-radius: 8px;
    width: 1000px;
    margin-top: -10px;
    box-shadow: 0px 0px 6px #0000004d;
    padding: 80px 140px;
    box-sizing: border-box;

    h2 {
        text-align: center;
        font-size: 35px;
        letter-spacing: -0.88px;
        color: #63c7a9;
        text-transform: uppercase;
        font-weight: bold;
        padding: 0px 0 40px 0px;
    }
    p {
        color: #363636;
        font-size: 16px;
    }
@media(max-width:1024px){
    height: auto;
    border-radius: 8px;
    width: 90%;
    margin-top: -10px;
    box-shadow: 0px 0px 6px #0000004d;
    padding: 60px 10px;
    box-sizing: border-box;

}
`;
export const Form = styled.form`
    background: #fff;
    display: flex;
    flex-wrap: wrap;
    margin: 0 auto;
    flex-flow: row wrap;
    justify-content: center;

button{
    clear:both;
    background: #63C7A9 0% 0% no-repeat padding-box;
    width:250px;
    color:#fff;
    outline:none;
    text-transform:uppercase;
    font-size:18px;
    text-align:center;
    border:none;
    padding:12px;
    margin-top:50px;
    position:relative;
    left:-150px;
    cursor: pointer;

    &:hover{
        background:#363636;
    }


    @media(max-width:1024px){
    width:200px;
    color:#fff;
    outline:none;
    text-transform:uppercase;
    font-size:18px;
    text-align:center;
    border:none;
    padding:12px;
    margin-top:20px;
    position:relative;
    left:0

    }
}
`;
export const InputForm = styled.div`
    margin-top: 30px;
    display: flex;
    justify-content: center;
    input {
        background: none;
        border: 1px solid #363636;
        outline: none;
        padding: 12px;
        color: #363636;
        width: 230px;
        margin: 0 20px;
    }
`;

export const Textarea = styled.textarea`
    background: none;
    border: 1px solid #363636;
    outline: none;
    padding: 12px;
    color: #363636;
    clear: both;
    width: 528px !important;
    display: flex;
    margin-top: 30px;
`;


export const MensagemError = styled.div`
    position:absolute;
    color:red;
font-size:12px;
margin-top:20px;
text-align:left;
margin-left:90px;
border:1px solid red; 
padding:10px;
width:320px;
@media(max-width:1024px){

    margin-left:25px;
    margin-top:10px;
    width:60%;
}
`
export const MensagemSucesso = styled.div`
    position:absolute;
    color:#58B790;
font-size:12px;
margin-top:20px;
text-align:left;
margin-left:90px;
border:1px solid #58B790; 
padding:10px;
width:320px;
@media(max-width:1024px){

    margin-left:25px;
    margin-top:10px;
    width:60%;
}
`

